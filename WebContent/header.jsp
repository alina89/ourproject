<!-- Navigation: add to body -->
<nav class="navbar navbar-inverse navbar-fixed-top" id="header"
	role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.jsp"><i
				class="fa fa-globe fa-1x"></i> Volunteer Advisor</a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="recommend.jsp">Suggest a Place</a></li>
				<li><a href="volunteer.jsp">Find a Place</a></li>				
				<li><a href="contact.jsp">Contact Us</a></li>				
				<li><a href="signin.jsp" style="margin-left:280px">Log In</a></li>
				<li><a href="signup.jsp">Sign Up</a></li>
			</ul>
		</div>
	</div>
</nav>
<!----------------->