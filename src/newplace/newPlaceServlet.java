package newplace;


import java.io.InputStream;
import java.io.IOException;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.annotation.MultipartConfig;

import javax.servlet.http.Cookie;
//import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.servlet.http.HttpSession;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import newplace.PlaceBean;
import newplace.PlaceDAO;

@WebServlet("/uploadServlet")
@MultipartConfig(maxFileSize = 16177215)
// upload file's size up to 16MB
public class newPlaceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	
	
	

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public newPlaceServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		


		try {
			PlaceBean place = new PlaceBean();
			
			

			place.setPlacename(request.getParameter("name"));
			place.setPhone(request.getParameter("phone"));
			place.setEmail(request.getParameter("email"));
			place.setCountry(request.getParameter("country"));
			place.setCity(request.getParameter("city"));

			place.setMinimalAge(request.getParameter("age"));

			place.setDaysStart(request.getParameter("Mintime"));
			place.setDaysEnd(request.getParameter("Maxtime"));

			place.setLangs(request.getParameterValues("language"));
			place.setSkills(request.getParameterValues("skills"));
			place.setAreas(request.getParameterValues("causeareas"));
			place.setProvisions(request.getParameterValues("benefit"));
			

			String user_name = request.getParameter("user_name");
			place.setUserName(user_name);
			String text = request.getParameter("txtReview");
			place.setReview(text);
			
			place.setScore(Integer.parseInt((request.getParameter("score"))));
			
			//accepting image
			InputStream inputStream = null; // input stream of the upload file
			// obtains the upload file part in this multipart request
			Part filePart = request.getPart("pic");
			if (filePart != null) {
				// prints out some information for debugging
				System.out.println(filePart.getName());
				System.out.println(filePart.getSize());
				System.out.println(filePart.getContentType());

				// obtains input stream of the upload file
				inputStream = filePart.getInputStream();
				place.setImage(inputStream);
				
			}
			;


			place = PlaceDAO.login(place);
	
			response.sendRedirect("NewPlaceThankYou.jsp"); //final stage
			
		} catch (Throwable theException) {
			theException.printStackTrace(response.getWriter());
		}

	}


}