package signUp;

import java.sql.*;

public class UserDAO {
	static Connection currentCon = null;
	static ResultSet rs = null;	

	public static UserBean login(UserBean bean) {

		// preparing some objects for connection: username, password, country, city, age, languages, skills 
		Statement stmt = null;	
		
		String username = bean.getUsername();
		String password = bean.getPassword();
		String country = bean.getCountry();
		String city = bean.getCity();
		String age = bean.getAge();
		String[] langauge = bean.getLangs();
		String[] skills = bean.getSkills();		

		String searchQuery = "select * from users where username='" + username + "' AND password='" + password + "'";
		
				
		try {
			// connect to DB
			currentCon = ConnectionManager.getConnection();
			stmt = currentCon.createStatement();			
			rs = stmt.executeQuery(searchQuery);
			boolean more = rs.next();

			// if user does not exist set the isValid variable to true and add him
			if (!more) {
				System.out.println("User was not found, add him");
				bean.setExists(false);
				
				String insertUserString = "INSERT INTO users (`username`,`password`,`country`,`city`,`age`, `langs`, `skills`) VALUES (?,?,?,?,?,?,?)";
				
				PreparedStatement stmtAddUser = currentCon.prepareStatement(insertUserString, Statement.RETURN_GENERATED_KEYS);
				stmtAddUser.setString(1, username);
				stmtAddUser.setString(2, password);
				stmtAddUser.setString(3, country);
				stmtAddUser.setString(4, city);
				stmtAddUser.setString(5, age);
				
				
				//skills - multiple	
				String allskills = "";				
				if (skills != null) {
					int count = 0;
					for (int i=0; i<skills.length; i++) {						
						String real_Skill = "";
						String query = "SELECT * FROM skills WHERE id=" + skills[i];
						rs = stmt.executeQuery(query);						
						if (rs.next()) {
							count++;
							real_Skill = rs.getString("skill");	
							//first one or the only one
							if (count == 1) {
								allskills = allskills + "|" + real_Skill + "|";
							}
							else {
								allskills = allskills + " " + real_Skill + "|";
							}
						}																												
					}			
				}
				stmtAddUser.setString(7, allskills);				
				
				//language - multiple
				String alllangauge = "";
				if (langauge != null) {
					int count = 0;
					for (int i=0; i<langauge.length; i++) {
						if(count==1) {
							alllangauge = alllangauge+ "|" + langauge[i] + "|";	
						}else {
							alllangauge = alllangauge+ " " + langauge[i] + "|";
						}																													
					}			
				}
				stmtAddUser.setString(6, alllangauge);				
				
				stmtAddUser.executeUpdate();
				ResultSet rs = stmtAddUser.getGeneratedKeys();
				rs.next();

				int newId = rs.getInt(1);
				bean.setId(newId);
			}

			// if user exists set the isValid variable to false
			else {
				System.out.println("User was found in the DB");				
				bean.setExists(true);
			}
			
			
			
		}catch (Exception ex) {
			System.out.println("Log In failed: An Exception has occurred! "
					+ ex);
		}

		// some exception handling
		finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
				}
				rs = null;
			}

			if (stmt != null) {
				try {
					stmt.close();
				} catch (Exception e) {
				}
				stmt = null;
			}

			if (currentCon != null) {
				try {
					currentCon.close();
				} catch (Exception e) {
				}

				currentCon = null;
			}
		}

		return bean;
	}
}