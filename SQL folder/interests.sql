-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 16, 2015 at 10:36 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `volunteer`
--

-- --------------------------------------------------------

--
-- Table structure for table `interests`
--

CREATE TABLE IF NOT EXISTS `interests` (
  `area` text NOT NULL,
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `interests`
--

INSERT INTO `interests` (`area`, `id`) VALUES
('Advocacy & Human Rights', 1),
('Employment', 2),
('LGBT', 3),
('Children & Youth', 4),
('Seniors', 5),
(' Crisis Support', 6),
('Immigrants & Refugees', 7),
('Women', 8),
('Emergency & Safety', 9),
('Media & Broadcasting', 10),
('Environment', 11),
('Health & Medicine', 12),
('Computers & Technology', 13),
('Hunger', 14),
(' International', 15),
('Education', 16),
(' Animals', 17),
('Arts & Culture', 18),
('People with Disabilities', 19),
(' Politics', 20),
('Homeless & Housing', 21),
('Sports', 22),
('Veterans & Military Families', 23),
('Justice & Legal', 24),
('Community', 25),
('Disaster Relief', 26),
('Education & Literacy', 27),
(' Faith-Based', 28),
('Terrorism Victims', 29),
('Race & Ethnicity', 30),
('Orphans', 31),
('Sexual Assault Victims', 32);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
