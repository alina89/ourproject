<%@ page language="java" contentType="text/html; charset=windows-1255"
	pageEncoding="windows-1255"%>
<%@ page import="java.sql.*"%>

<%
	ResultSet resultset = null;
%>
<%
	ResultSet resultset2 = null;
%>
<%
	ResultSet resultset3 = null;
%>

<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Volunteer Advisor</title>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="css/heroic-features.css" rel="stylesheet">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/home.css">

<script src="js/jquery.js"></script>

</head>

<body>

	<!-- Should be available to all users -->

	<%
		String userName = null;
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals("user"))
					userName = cookie.getValue();
			}
	%>

	<nav class="navbar navbar-inverse navbar-fixed-top" id="header"
		role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.jsp"><i
					class="fa fa-globe fa-1x"></i> Volunteer Advisor</a>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="recommend.jsp">Suggest a Place</a></li>
					<li><a href="volunteer.jsp">Find a Place</a></li>
					<li><a href="contact.jsp">Contact Us</a></li>
					<li style="margin-left: 280px; color: #9d9d9d; margin-top: 15px;">Welcome
						<%=userName%>
					</li>
					<li><form style="margin-top: 11px; padding-left: 12px;"
							action="LogoutServlet" method="post">
							<input type="submit" value="Log out">
						</form></li>
				</ul>
			</div>
		</div>
	</nav>
	<%
		}
	%>
	<%
		if (userName == null) {
	%>
	<%@include file="header.jsp"%>
	<%
		}
	%>


	<div id="res" class="container"></div>
	<div id= "MainC" class="container">
		<!-- Page Content -->
		<!-- Jumbotron Header -->
		<header class="jumbotron hero-spacer">
			<h1>Let's change the world together!</h1>
			<p>Here you can find a place to volunteer, recommend about a new
				place and find what others think about volunteering.</p>
			<p>"Wherever a man turns he can find someone who needs him..."
				~Albert Schweitzer</p>
		</header>

		<hr>

		<!-- Title -->
		<div class="row">
			<div class="col-lg-12">
				<h3>Popular Places</h3>
			</div>
		</div>
		<!-- /.row -->

		<!-- Page Features -->
		<div class="row text-center">
			<%{
				Connection con = null;
				Class.forName("com.mysql.jdbc.Driver").newInstance();
				con = DriverManager.getConnection("jdbc:mysql://localhost/volunteer", "root", "");
				Statement statement1 = con.createStatement();
				resultset = statement1.executeQuery("select places.*, avg(reviews.score) as avgscore from places inner join reviews on places.id = reviews.place_id group by place_id order by avg(reviews.score) limit 4");
				
				int i = 0;
				while (resultset.next()) {%>					
					<%i++; %>
					<div class="col-md-3 col-sm-6 hero-feature">
						<div class="thumbnail">						
							<img src="image?place_id=<%=resultset.getInt("id")%>" style="width:auto; height:160px" alt="">
							<div class="caption">
								<h3><%= resultset.getString("name") %></h3>
								<p><%= resultset.getString("country")%>, <%= resultset.getString("city")%></p>
								<p>
									<a href="placePage.jsp?id=<%= resultset.getInt("id") %>" class="btn btn-primary">More Info</a>
								</p>
							</div>
						</div>
					</div>					
				<%}
			}%>

		</div>
		<!-- /.row -->

		<hr>
		<h3>Quick Search</h3>

		<div class="col-lg-6" id="mainForm">
			<div class="well bs-component">
				<form id="find" class="form-horizontal">
					<div class="form-group">
						<label for="inputCountry" class="col-lg-2 control-label">Country</label>
						<div class="col-lg-10">
							<input type="text" class="form-control"
								placeholder="Enter Country name to see autocomplete"
								id="inputString" name="country" />
						</div>
					</div>
					<div class="form-group" id="city" style="display: none;">
						<label for="inputCity" class="col-lg-2 control-label">City</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="inputString2"
								name="city" placeholder="Enter City name to see autocomplete">
						</div>
					</div>
					<div class="form-group">
						<label for="select" class="col-lg-2 control-label">Area of
							Interest </label>
						<div class="col-lg-10">

							<%
								try {
									Connection con = null;
									Class.forName("com.mysql.jdbc.Driver").newInstance();
									con = DriverManager.getConnection("jdbc:mysql://localhost/volunteer", "root", "");
									Statement statement1 = con.createStatement();
									resultset = statement1.executeQuery("select * from interests");
							%>


							<select class="form-control" name="skills">
								<%
									while (resultset.next()) {
								%>
								<option><%=resultset.getString(1)%></option>
								<%
									}
								} catch (Exception e) {
									out.println("Wrong Entry: " + e);
								}
								%>
							</select>
						</div>
					</div>
					<input style="margin-left: 665px;" type="submit" value="Search" />
				</form>
			</div>
			<br>
		</div>
		<!-- Footer -->
		<%@include file="footer.jsp"%>
	</div>

	<!-- /.container -->

	<!-- jQuery -->
	<script src="js/jquery.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="js/jquery.autocomplete.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/index.js"></script>

</body>

</html>
