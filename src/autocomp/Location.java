package autocomp;

public class Location {
	private int id;
	private String name;
	private int type;
	
	public Location(int id, String name, int type) {
		this.setId(id);
		this.setName(name);
		this.setType(type);
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	

}
