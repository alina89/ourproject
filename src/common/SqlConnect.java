package common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SqlConnect {

	public Connection getConn() throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException  {
		Connection con = null;
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		con = DriverManager.getConnection("jdbc:mysql://localhost/volunteer", "root", "");
		return con;
	}
}