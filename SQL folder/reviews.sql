-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 16, 2015 at 10:37 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `volunteer`
--

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE IF NOT EXISTS `reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `place_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `review` text NOT NULL,
  `created_ts` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `place_id` (`place_id`,`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=63 ;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `place_id`, `user_id`, `score`, `review`, `created_ts`) VALUES
(44, 4, 39, 5, '', '2015-06-16 16:05:57'),
(45, 6, 39, 10, '', '2015-06-16 16:06:36'),
(46, 4, 40, 5, '', '2015-06-16 16:06:49'),
(47, 6, 40, 10, '', '2015-06-16 16:06:59'),
(48, 4, 41, 5, '', '2015-06-16 16:07:10'),
(49, 6, 41, 10, '', '2015-06-16 16:07:20'),
(50, 4, 42, 5, '', '2015-06-16 16:07:36'),
(51, 6, 42, 1, '', '2015-06-16 16:07:59'),
(52, 4, 43, 5, '', '2015-06-16 16:08:12'),
(53, 6, 43, 1, '', '2015-06-16 16:08:22'),
(54, 4, 44, 5, '', '2015-06-16 16:08:33'),
(55, 6, 44, 1, '', '2015-06-16 16:08:42'),
(56, 2, 22, 10, '', '2015-06-16 16:10:41'),
(57, 2, 30, 8, 'great place!', '2015-06-16 16:11:11'),
(58, 1, 10, 7, '', '2015-06-16 16:12:07'),
(59, 1, 26, 8, '', '2015-06-16 16:12:21'),
(60, 9, 11, 6, '', '2015-06-16 16:16:51'),
(61, 8, 15, 7, '', '2015-06-16 16:17:42'),
(62, 8, 18, 8, '', '2015-06-16 16:18:12');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
