<%@ page import="java.sql.*"%>
<%
	ResultSet resultset1 = null;
	ResultSet resultset2 = null;
	ResultSet resultset3 = null;
	ResultSet resultset4 = null;
	ResultSet resultset5 = null;
%>
<%@ page language="java" contentType="text/html; charset=windows-1255"
	pageEncoding="windows-1255"%>




<!DOCTYPE html>
<html lang="en">

<head>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1255">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Volunteer Advisor</title>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="css/heroic-features.css" rel="stylesheet">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<script src="js/jquery.js"></script>
<script src="js/jquery.raty.js"></script>
<link rel="stylesheet" href="css/style.css" />


</head>

<body>

	<!-- Should be available only to logged in users -->

	<%
		String userName = null;
		int userId = -1;
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals("user"))
					userName = cookie.getValue();
				else if (cookie.equals("user_id"))
					userId = Integer.parseInt(cookie.getValue());
			}
	%>

	<nav class="navbar navbar-inverse navbar-fixed-top" id="header"
		role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.jsp"><i
					class="fa fa-globe fa-1x"></i> Volunteer Advisor</a>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="recommend.jsp">Suggest a Place</a></li>
					<li><a href="volunteer.jsp">Find a Place</a></li>
					<li><a href="contact.jsp">Contact Us</a></li>
					<li style="margin-left: 280px; color: #9d9d9d; margin-top: 15px;">Welcome
						<%=userName%>
					</li>
					<li><form style="margin-top: 11px; padding-left: 12px;"
							action="LogoutServlet" method="post">
							<input type="submit" value="Log out">
						</form></li>
				</ul>
			</div>
		</div>
	</nav>
	<%
		}
	%>
	<%
		if (userName == null)
			response.sendRedirect("signin.jsp");
	%>


	<div class="container">
		<form method="post" action="newPlaceServlet"
			enctype="multipart/form-data">
			<header>
				<h1>
					<span>Recommend a place to volunteer!</span>
				</h1>
			</header>

			<p>
				<b>Please fill in relevant details below. Feel free to add a
					picture and a personal review</b>.<br>
			</p>
			<div class="form">
					<p class="form-group">
						<label for="name">Name of the place</label>
					</p>
					<input id="name" name="name" placeholder="Please enter a name"
						required="" tabindex="1" type="text">

					<p class="form-group">
						<label for="phone">Phone number</label>
					</p>
					<input id="phone" name="phone"
						placeholder="Please enter a valid number" required="" type="text">

					<p class="form-group">
						<label for="email">Email address</label>
					</p>
					<input id="email" name="email" placeholder="who@where.com"
						required="" type="text">

					<div class="form-group">
						<p>
							<label for="inputCountry" class="contact">Country</label>
						</p>

						<input type="text" class="form-control"
							placeholder="Enter Country name to see autocomplete"
							id="inputString" name="country" />

						<div class="suggestionsBox" id="suggestions"
							style="display: none;">
							<div class="suggestionList" id="autoSuggestionsList"></div>
						</div>
					</div>

					<div class="form-group" id="city" style="display: none;">
						<p>
							<label for="inputCity" class="contact">City</label>
							<!-- Stup -->
						</p>
						<!-- <div class="col-lg-10"> -->
						<!-- Stup -->
						<input type="text" class="form-control" id="inputString2"
							name="city" placeholder="Enter City name to see autocomplete" />
						<div class="suggestionsBox2" id="suggestions2"
							style="display: none;">
							<div class="suggestionList2" id="autoSuggestionsList2"></div>
						</div>
						<!-- </div> -->
						<!-- Stup -->
					</div>



					<p class="form-group">
						<label for="phone">Related Cause Areas</label>
					</p>

					<table id="causeareas">
						<tbody>
							<%
								try {
									Connection connection = DriverManager.getConnection(
											"jdbc:mysql://localhost/volunteer", "root", "");
									Statement statement1 = connection.createStatement();
									resultset1 = statement1.executeQuery("select * from interests");
									Statement statement2 = connection.createStatement();
									resultset2 = statement2.executeQuery("select * from skills");
									Statement statement3 = connection.createStatement();
									resultset3 = statement3.executeQuery("select * from languages");
									Statement statement4 = connection.createStatement();
									resultset4 = statement4.executeQuery("select * from benefits");

									/* cause areas handling */
							%>
							<%
								int countca = 1;
									while (resultset1.next()) {
										if (countca % 3 == 1) {
							%>
							<tr>
								<%
									}
								%>
								<td width="208px">
									<p>
										<input type="checkbox" name="causeareas"
											value=<%=resultset1.getString(2)%>>&nbsp;&nbsp;<label><%=resultset1.getString(1)%></label>
									</p>
								</td>
								<%
									if (countca % 3 == 0) {
								%>
							</tr>
							<%
								}
							%>
							<%
								countca++;
									}
							%>


							<%
								} catch (Exception e) {
									out.println("wrong entry" + e);
								}
							%>
						</tbody>
					</table>


					<fieldset>
						<label>Minimal Duration (days) <input class="birthday"
							maxlength="2" name="Mintime" placeholder="1" required=""></label>
						<label></label><label class="month"></label>
					</fieldset>


					<fieldset>
						<label>Maximal Duration (days) <input class="birthday"
							maxlength="2" name="Maxtime" placeholder="30" required=""></label>
						<label></label><label class="month"> </label>
					</fieldset>


					<p class="form-group">
						<label for="location">Required Skills</label>
					</p>

					<table id="skills">
						<tbody>

							<%
								int countca = 1;
								while (resultset2.next()) {
									if (countca % 3 == 1) {
							%>
							<tr>
								<%
									}
								%>
								<td width="208px">
									<p>
										<input type="checkbox" name="skills"
											value=<%=resultset2.getString(2)%>>&nbsp;&nbsp;<label><%=resultset2.getString(1)%></label>
									</p>
								</td>
								<%
									if (countca % 3 == 0) {
								%>
							</tr>
							<%
								}
							%>
							<%
								countca++;
								}
							%>

						</tbody>
					</table>

					<div class="form-group">
						<label for="select" class="contact">Required Language</label> <select
							multiple="" class="form-control" name="language">
							<%
								while (resultset3.next()) {
							%>
							<option><%=resultset3.getString(1)%></option>
							<%
								}
							%>
						</select>
					</div>


					<div class="form-group">
						<!-- <p class="contact"> -->
						<label for="select">Minimal Age: </label>
						<!-- </p> -->
						<select class="form-control" name="age">
							<%
								int age = 8;
								while (age < 76) {
							%>
							<option><%=age++%></option>
							<%
								}
							%>
						</select>
					</div>

					<div class="form-group">
						<label for="select" class="contact">This place provides:</label>
						<!-- <div class="col-lg-10"> -->

						<select multiple="" class="form-control" name="benefit">
							<%
								while (resultset4.next()) {
							%>
							<option><%=resultset4.getString(2)%></option>
							<%
								}
							%>
						</select>
						<!-- </div> -->
					</div>


					<p class="form-group">
						<label for="location">Rate the place: </label>
					</p>
					<div id="newRating"></div>
					<br>
					<p class="form-group">
						<label for="review">Add your personal review</label>
					</p>

					<textarea id="txtReview" name="txtReview"
						style="width: 670px; height: 100px"></textarea>

					<input type="hidden" name="user_name" value=<%=userName%>>

					<p class="form-group">
						<label for="repassword">Add images</label>
					</p>

					<input type="file" name="pic" placeholder="Upload Your Image"
						accept="image/*"> <input type="submit" value="Send!"
						class="btn btn-primary btn-block btn-lg" tabindex="5">
			</div>
			<hr>
		</form>
		<!-- Footer -->
		<%@include file="footer.jsp"%>
	</div>


	<script src="js/jquery.autocomplete.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/volunteer.js"></script>
	<script src="js/recommend.js"></script>

</body>

</html>


