

//adds 10 stars that the user can rate
$('#newRating').raty({ number: 10, path: 'images' });

//gets the score and the review text when the "add review" button is pushed
$('#newReviewBtn').click(function() {
	var review = {};
	review.score = $("#newRating").raty('score');
	review.review = $("#txtReview").val();
	review.placeId = getParameterByName("id"); 
	$.post('reviews', review, function onSaveComplete(result) {
		console.log(result);
	});
});