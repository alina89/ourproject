package newplace;

import java.io.InputStream;

public class PlaceBean {
	//placename, phone, email, country, city, areas, skills, age_start, age_end, rating, provisions, days_start, days_end, review, image
	private String user_name;
	private int userid;
	private int place_id;
	private int score;
	private String review;
	private String placename;
	private String phone;
	private String email;
	private String country;
	private String city;
	private String minimal_age;
	private String days_start;
	private String days_end;
	//should change array types of skills and area to int - id instead of names
	private String[] langs;
	private String[] skills;
	private String[] areas;
	private String[] provisions;
	
	private InputStream image = null;

	
	public String getUserName() {
		return user_name;
	}

	public void setUserName(String newusername) {
		this.user_name = newusername;
	}	
	
	public int getUserId() {
		return userid;
	}

	public void setUserId(int newuserid) {
		this.userid = newuserid;
	}	
	
	public int getPlaceId() {
		return place_id;
	}

	public void setPlacId(int newplaceid) {
		this.place_id = newplaceid;
	}
	
	public int getScore() {
		return score;
	}

	public void setScore(int newscore) {
		this.score = newscore;
	}
	
	public String getReview() {
		return review;
	}

	public void setReview(String newreview) {
		review = newreview;
	}
	
	
	public String getPlacename() {
		return placename;
	}

	public void setPlacename(String newPlacename) {
		placename = newPlacename;
	}
		

	public String getPhone() {
		return phone;
	}

	public void setPhone(String newPhone) {
		phone = newPhone;
	}

	public void setEmail(String newEmail) {
		email = newEmail;
	}
	public String getEmail() {
		return email;
	}


	public String getCountry() {
		return country;
	}

	public void setCountry(String newCountry) {
		country = newCountry;
	}
	
	public String getCity() {
		return city;
	}

	public void setCity(String newCity) {
		city = newCity;
	}
	
	public String getMinimalAge() {
		return minimal_age;
	}

	public void setMinimalAge(String newMinimalAge) {
		minimal_age = newMinimalAge;
	}

	public String getDaysStart() {
		return days_start;
	}

	public void setDaysStart(String newDaysStart) {
		days_start = newDaysStart;
	}
	public String getDaysEnd() {
		return days_end;
	}

	public void setDaysEnd(String newDaysEnd) {
		days_end = newDaysEnd;
	}

	
	public InputStream getImage() {
		return image;
	}

	public void setImage(InputStream newImage) {
		image = newImage;
	}
	
	
	public String[] getLangs() {
		return langs;
	}

	public void setLangs(String[] newLangs) {
		langs = newLangs;
	}
	
	public String[] getSkills() {
		return skills;
	}

	public void setSkills(String[] newSkills) {
		skills = newSkills;
	}
	
	public String[] getAreas() {
		return areas;
	}

	public void setAreas(String[] newAreas) {
		areas = newAreas;
	}
	
	public String[] getProvisions() {
		return provisions;
	}

	public void setProvisions(String[] newProvisions) {
		provisions = newProvisions;
	}

}
