//dynamically adds the search results and removes the search form 
$("#find").on("submit", function( event ) {
	event.preventDefault();
	var params = $.param($(this).serializeArray());
	$("#MainC").css("display", "none");
	$.get("places", params, function(data) {
		
		//show how many places have been found - create each row 
		for (var j=0; j<data.length; j++) {
			
			//Main row
			var divRow = document.createElement('div');		
			divRow.className = 'row';	
			
			var res = document.getElementById('res');
			res.appendChild(divRow);
			
			//image part
			var divImg = document.createElement('div');		
			divImg.className = 'col-md-7';
			
			var a = document.createElement('a');
			a.href = "#";
			
			var oImg=document.createElement("img");
			oImg.className = "img-responsive";
			oImg.src = data[j].image_url;
			
			divImg.appendChild(oImg);
			
			//data part
			var divData = document.createElement('div');		
			divData.className = 'col-md-5';
			
			var h3 = document.createElement('h3');
			h3.innerHTML = data[j].name;
			var h4 = document.createElement('h4');
			h4.innerHTML = "Phone number: " + data[j].phone;

			
			var a2 = document.createElement('a');
			a2.href = "placePage.jsp" + "?id=" + data[j].id;
			a2.innerHTML = "Learn more";
			a2.className = "btn btn-primary";
			
			var span = document.createElement('span');
			span.className = "glyphicon glyphicon-chevron-right";
			a2.appendChild(span);
			
			divData.appendChild(h3);
			
			//TODO: add avg rating			
//			var divRating = document.createElement('div');
//			divRating.className = "rating";
//			for (var i=0; i<data[j].rating; i++) {	
//				var temp = document.createElement('span');
//				temp.innerHTML = "<i class='fa fa-star'></i>";
//				divRating.appendChild(temp);
//			}
//			
//			divData.appendChild(divRating);
			divData.appendChild(h4);
			divData.appendChild(a2);
			
			divRow.appendChild(divImg);
			divRow.appendChild(divData);
			
			if (!(data.length ==1))	{
				var hr = document.createElement('hr');
				res.appendChild(hr);
			}
			
			var br = document.createElement('br');			
			res.appendChild(br);
		}			
	});
});


//the auto complete function that fills the countries + cities (sends the request to the server)
$(function() {
	var cityInput = $('[name="city"]'),
		countryInput = $('[name="country"]');
	
	countryInput.autocomplete({
	    serviceUrl: 'countries',
	    onSelect: function (suggestion) {
	    	cityInput.val('');
	    	var countryId = suggestion.data;
	    	cityInput.autocomplete({
	    	    serviceUrl: 'cities',
	    	    params: { country_id: countryId }
	    	});
	    	$('#city').show();
	    }
	})
	.blur(function() {
		if ($(this).val() === '')
			$('#city').hide();
	});
});