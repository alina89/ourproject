package autocomp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import common.SqlConnect;

public class LocationDAO {
	
	//pulls the list of the countries from the DB
	public static List<Location> searchCountries(String term) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		List<Location> results = new ArrayList<Location>();
		
        Connection con = new SqlConnect().getConn();
        String sqlTerm = String.format("%%%s%%", term);
        PreparedStatement ps = con.prepareStatement("SELECT * FROM location WHERE name like (?) AND location_type=0");
        ps.setString(1, sqlTerm); //puts the country in the (?) above
        ResultSet rs = ps.executeQuery();
        
        while (rs.next()) {
        	int id = rs.getInt("location_id");
        	int type = rs.getInt("location_type");
        	String name = rs.getString("name");
        	
        	results.add(new Location(id, name, type));
        }
        
		return results;
	}
	
	//pulls the list of cities from the DB according to the correct country 
	public static List<Location> searchCities(String term, int countryId) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		List<Location> results = new ArrayList<Location>();
		
        Connection con = new SqlConnect().getConn();
        String sqlTerm = String.format("%%%s%%", term);
        
        PreparedStatement ps = con.prepareStatement("SELECT cities.* FROM location as cities inner join location as states on cities.parent_id = states.location_id    WHERE states.parent_id = (?) AND cities.name like (?)");
        ps.setInt(1, countryId); //puts the country in the first (?) above
        ps.setString(2, sqlTerm); //puts the parent in the second (?) above

        ResultSet rs = ps.executeQuery();
        
        while (rs.next()) {
        	int id = rs.getInt("location_id");
        	int type = rs.getInt("location_type");
        	String name = rs.getString("name");
        	
        	results.add(new Location(id, name, type));
        }
        
		return results;
	}
}
