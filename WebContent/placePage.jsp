<%@ page import="java.sql.*"%>
<%@ page import="java.io.*"%>
<%@ page import="common.SqlConnect" %>
<!DOCTYPE html>
<html lang="en">
<%
	ResultSet rs = null;
	PreparedStatement ps = null;	
	String query = "";	
	String userName = null;
	int userId = -1;
	boolean found = false;
	int id = Integer.parseInt(request.getParameter("id"));
	Connection con = new SqlConnect().getConn();
%>

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Shop Item - Start Bootstrap Template</title>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="css/shop-item.css" rel="stylesheet">
<link href="css/jquery.raty.css" rel="stylesheet">

</head>

<body>
	<%
		query = "SELECT avg(score) FROM reviews WHERE place_id=?;";
		ps = con.prepareStatement(query);
		ps.setInt(1, id);
		rs = ps.executeQuery();
		found = rs.next();	

		float average = rs.getFloat(1);
	
	%>
	<script type="text/javascript">
		window.averageScore = <%= average %>;
	</script>

	<!-- Should be available only to logged in users -->

	<%		
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				String cookieName = cookie.getName();
				if (cookieName.equals("user"))
					userName = cookie.getValue();
				else if (cookieName.equals("user_id"))
					userId = Integer.parseInt(cookie.getValue());
			}
		}
		if (userName != null) {
	%>

	<nav class="navbar navbar-inverse navbar-fixed-top" id="header"
		role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.jsp"><i
					class="fa fa-globe fa-1x"></i> Volunteer Advisor</a>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><form style="margin-top: 11px; padding-left: 12px;"
							action="LogoutServlet" method="post">
							<input type="submit" value="Log out">
						</form></li>
					<li style="margin-left: 280px; color: #9d9d9d; margin-top: 15px;">Welcome
						<%=userName%>
					</li>
					<li><a href="contact.jsp">Contact Us</a></li>
					<li><a href="volunteer.jsp">Find a Place</a></li>
					<li><a href="recommend.jsp">Suggest a Place</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<%
		}
		else {
	%>
	<%@include file="header.jsp"%>
	<%
		}
	%>
	


	<!-- Page Content -->	
	<div class="container">

		<%			
			// Get a Connection to the database			
			query = "SELECT * FROM places WHERE id=?;";
			ps = con.prepareStatement(query);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			found = rs.next();	
			
			String query2 = "SELECT *, u.username FROM reviews r inner join Users u on r.user_id=u.id WHERE place_id=? ORDER BY created_ts DESC;";
			PreparedStatement ps2 = con.prepareStatement(query2);
			ps2.setInt(1, id);
			ResultSet rs2 = ps2.executeQuery();	
		%>
		<div id="res"></div>
		<div class="row">
			<div class="col-md-9">
				<div class="thumbnail">
					<img class="img-responsive" src="image?place_id=<%=id%>"
						alt="">
						
					<div class="caption-full">
						<h4 class="pull-right">
							<%= rs.getString("phone") %>
						</h4>
						<h4>
							<a href="#"> <%= rs.getString("name") %>
							</a>
						</h4>
						<p>email</p>
						<p>
							Country:
							<%=	rs.getString("country") %>
							City:
							<%=	rs.getString("city") %>
						</p>
						<p>
							Required Skills:
							<%=	rs.getString("skills") %>
						</p>
						<p>
							Minimum age:
							<%= rs.getString("age_start") %>
							+
						</p>
						<p>
							Min duration:
							<%= rs.getString("days_start") %>
						</p>
						<p>
							Languages:
							<%= rs.getString("lang") %>
						</p>
						<p>
							The place offers:
							<%= rs.getString("looking_for") %>
						</p>
						<div class="ratings">
							<p>
							Average Score: <%= average %>  
							<div id="avgScore"></div>	
							</p>
						</div>
					</div>
					<div class="well">
						<%
							query = "SELECT * FROM reviews WHERE place_id=? AND user_id=?;";
							PreparedStatement psUserReviewExists = con.prepareStatement(query);
							psUserReviewExists.setInt(1, id);
							psUserReviewExists.setInt(2, userId);
							ResultSet rsUserReviewExists = psUserReviewExists.executeQuery();
							if (!rsUserReviewExists.next() && userName != null) {
						%>
						<div id="newRatingContainer">
							Rate the place:
							<div id="newRating"></div>
							<br />
							<textarea id="txtReview" style="width: 670px; height: 100px"></textarea>
							<a id="newReviewBtn" class="btn btn-success pull-right">Leave
								a Review</a>
						</div>
						
						<% 	} %>

						<hr>

						<div id="mainRow" class="row">
							<div id= "MainClass" class="col-md-12">
								<% while (rs2.next()) { %>									
									
								<div class="row">
									<div class="col-md-12">
									<%=rs2.getString("username") %> 
									<%for (int i=0; i < rs2.getInt("score"); i++) { %>
										<span class="glyphicon glyphicon-star"></span> 
									<%}%> 
									
									<%for (int j=0; j < (10 - rs2.getInt("score")); j++) { %>
										<span class="glyphicon glyphicon-star-empty"></span>
									<%} %>										
										
										
										<p><%=rs2.getString("review") %></p>
										<hr>
									</div>
								</div>


								<% }
						
						%>								
							</div>
						</div>

						

					</div>
				</div>
			</div>
		</div>
	</div>
	

	<!-- /.container -->

	
	<div class="container">
		<%@include file="footer.jsp"%>
	</div>
	<!-- /.container -->

	<!-- jQuery -->
	<script src="js/jquery.js"></script>
	<script src="js/jquery.raty.js"></script>
	
	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.js"></script>

	<script src="js/placepage.js"></script>
	
</body>

</html>
