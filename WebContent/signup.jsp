<%@ page language="java" contentType="text/html; charset=windows-1255"
	pageEncoding="windows-1255"%>
<%@ page import="java.sql.*"%>
<%
	ResultSet resultset2 = null;
%>
<%
	ResultSet resultset3 = null;
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1255">
<title>Volunteer Advisor</title>

<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/signup.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
</head>

<body>	
	<div class="container">

		<div class="row">
			<div
				class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
				<form action="SignUpServlet">
					<h2>
						Please Sign Up <small>It's free and always will be.</small>
					</h2>
					<hr class="colorgraph">

					<!-- Display Name -->
					<div class="form-group">
						<input type="text" name="display_name" id="display_name"
							class="form-control input-lg" placeholder="Display Name"
							tabindex="1">
					</div>

					<!-- Password -->
					<div class="form-group">
						<input type="password" name="password" id="password"
							class="form-control input-lg" placeholder="Password" tabindex="2">
					</div>

					<!-- Country of Origin -->
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="form-group">
								<input type="text" name="country" id="first_name"
									class="form-control input-lg" placeholder="Country of Origin"
									tabindex="3">
							</div>
						</div>

						<!-- City of Origin -->
						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="form-group">
								<input type="text" name="city" id="city"
									class="form-control input-lg" placeholder="City of Origin"
									tabindex="4">
							</div>
						</div>
					</div>

					<!-- Age -->
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="form-group">
								<p class="ageClass" id="age">Choose your age:</p>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="form-group">
								<select class="form-control input-lg" name="age">
									<%
										int age = 8;
										while (age < 76) {
									%>
									<option><%=age++%></option>
									<%
										}
									%>
								</select>
							</div>
						</div>
					</div>

					<!-- Languages -->
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="form-group">
								<p class="ageClass" id="language">Choose your languages:</p>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="form-group">
								<select multiple="" id="multiSelect" class="form-control" name="language">
									<%
										try {
											Class.forName("com.mysql.jdbc.Driver").newInstance();
											Connection con = DriverManager.getConnection(
													"jdbc:mysql://localhost/volunteer", "root", "");
											Statement statement2 = con.createStatement();
											resultset2 = statement2.executeQuery("select * from skills");
											Statement statement3 = con.createStatement();
											resultset3 = statement3.executeQuery("select * from languages");
									%>
									<%
										} catch (Exception e) {
											out.println("Wrong Entry: " + e);
										}
									%>
									<%
										while (resultset3.next()) {
									%>
									<option><%=resultset3.getString(1)%></option>
									<%
										}
									%>

								</select>
							</div>
						</div>
					</div>

					<!-- Skills -->
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="form-group">
								<p class="ageClass" id="skills">Choose your skills:</p>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="form-group">
							<table id="causeareas">
								<tbody>
									<%
										int countca = 1;
										while (resultset2.next()) {
											if (countca % 3 == 1) {
									%>
									<tr>
										<%
											}
										%>
										<td width="250px">
											<p>
												<input type="checkbox" name="skill"
													value=<%=resultset2.getString(2)%>>&nbsp;&nbsp;<label
													class="skill"><%=resultset2.getString(1)%></label>
											</p>
										</td>
										<%
											if (countca % 3 == 0) {
										%>
									</tr>
									<%
										}
									%>
									<%
										countca++;
										}
									%>
								</tbody>
							</table>
						</div>

					</div>

					<!-- Register or Sign in -->
					<hr class="colorgraph">
					<div class="row">
						<div class="col-xs-12 col-md-6">
							<input type="submit" value="Register"
								class="btn btn-primary btn-block btn-lg" tabindex="5">
						</div>
						<div class="col-xs-12 col-md-6">
							<a href="index.jsp" class="btn btn-success btn-block btn-lg">Cancel</a>
						</div>
					</div>
				</form>
			</div>
		</div>		
	</div>


	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/volunteer.js"></script>
	<script src="js/jquery.autocomplete.min.js"></script>
	
	
</body>
</html>