package reviews;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import common.SqlConnect;

public class ReviewDAO {

	public static ReviewBean saveReview(int placeId, String review, int score, int userId) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		ReviewBean result = new ReviewBean();
		result.setPlaceId(placeId);
		result.setUserId(userId);
		result.setReview(review);
		result.setScore(score);

		String insertReview = "INSERT INTO reviews (`place_id`,`review`,`score`,`user_id`) VALUES (?,?,?,?)";
		
		Connection conn = new SqlConnect().getConn();
		PreparedStatement stmt = conn.prepareStatement(insertReview, Statement.RETURN_GENERATED_KEYS);
		
		stmt.setInt(1, placeId);
		stmt.setString(2, review);
		stmt.setInt(3, score);
		stmt.setInt(4, userId);
		
		stmt.executeUpdate();
		ResultSet rs = stmt.getGeneratedKeys();
		rs.next();

		int reviewId = rs.getInt(1);
		result.setId(reviewId);
		
		return result;
	}
}
