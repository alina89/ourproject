//returns the place id - passed through the request href URL
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

$("#avgScore").raty({ number: 10, path: 'images', score: window.averageScore, readOnly: true, precision: true });

//adds 10 stars that the user can rate
$('#newRating').raty({ number: 10, path: 'images' });

//gets the score and the review text when the "add review" button is pushed
$('#newReviewBtn').click(function() {
	var review = {};
	review.score = $("#newRating").raty('score');
	review.review = $("#txtReview").val();
	review.placeId = getParameterByName("id"); 
	$.post('reviews', review, function onSaveComplete(result) {
		console.log(result);
		$("#newRatingContainer").hide();
		var divRow = document.createElement('div');		
		divRow.className = 'row';	
		
		var divCol = document.createElement('div');		
		divCol.className = 'col-md-12';
		divCol.innerHTML = result.userId;
			
		divRow.appendChild(divCol);
		$(divRow).append(" ");
		
		for(var i=0; i <review.score; i++) {
			var spany = document.createElement('span');		
			spany.className = 'glyphicon glyphicon-star';
			divCol.appendChild(spany);
			$(divCol).append(" ");
		}
		for (var j=0; j<(10 - review.score); j++) {
			var spanyy = document.createElement('span');		
			spanyy.className = 'glyphicon glyphicon-star-empty';
			divCol.appendChild(spanyy);
			$(divCol).append(" ");
		}
		
		var p = document.createElement('p');
		p.innerHTML = review.review;
		divCol.appendChild(p);
		$(divCol).append("<hr>");
		
		$("#MainClass").prepend(divRow);

	});
});