<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="java.sql.*"%>
<%
	ResultSet resultset = null;
%>
<%
	ResultSet resultset2 = null;
%>
<%
	ResultSet resultset3 = null;
%>

<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>Volunteer Advisor</title>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="css/heroic-features.css" rel="stylesheet">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/style.css" />

<script src="js/jquery.js"></script>
</head>



<body>

<!-- Should be available only to logged in users -->

	<%
		String userName = null;
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals("user"))
					userName = cookie.getValue();
			}
	%>

	<nav class="navbar navbar-inverse navbar-fixed-top" id="header"
		role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.jsp"><i
					class="fa fa-globe fa-1x"></i> Volunteer Advisor</a>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="recommend.jsp">Suggest a Place</a></li>
					<li><a href="volunteer.jsp">Find a Place</a></li>					
					<li><a href="contact.jsp">Contact Us</a></li>
					<li style="margin-left:280px; color:#9d9d9d; margin-top:15px;">Welcome <%=userName%>
					</li>
					<li><form style="margin-top:11px; padding-left:12px;" action="LogoutServlet" method="post">
							<input type="submit" value="Log out">
						</form></li>
				</ul>
			</div>
		</div>
	</nav>
	<%
		}
	%>
	<%
		if (userName == null)
			response.sendRedirect("signin.jsp");
	%>
	

	<div class="container">
		<header>
			<h1>
				<span>Volunteer - </span> Find the right place for you!
			</h1>
		</header>

		<br> <br>

		<div id="res"></div>
		<div class="col-lg-6" id="mainForm">
			<div class="well bs-component">
				<form id="find" class="form-horizontal">
					<fieldset>
						<legend>
							<i class="fa fa-question-circle fa-3x" style="color: #337ab7"></i>
						</legend>
						<div class="form-group">
							<label for="inputCountry" class="col-lg-2 control-label">Country</label>
							<div class="col-lg-10">
								<input type="text" class="form-control"
									placeholder="Enter Country name to see autocomplete"
									id="inputString" name="country"
									 />
							</div>
							<div class="suggestionsBox" id="suggestions"
								style="display: none;">
								<div class="suggestionList" id="autoSuggestionsList"></div>
							</div>
						</div>
						<div class="form-group" id="city" style="display: none;">
							<label for="inputCity" class="col-lg-2 control-label">City</label>
							<div class="col-lg-10">
								<input type="text" class="form-control" id="inputString2"
									name="city" placeholder="Enter City name to see autocomplete">
								<div class="suggestionsBox2" id="suggestions2"
									style="display: none;">
									<div class="suggestionList2" id="autoSuggestionsList2"></div>
								</div>
							</div>
						</div>


						<div class="form-group">
							<label for="select" class="col-lg-2 control-label">Area of Interest
								</label>
							<div class="col-lg-10">

								<%
									try {
										Class.forName("com.mysql.jdbc.Driver").newInstance();
										Connection con = DriverManager.getConnection(
												"jdbc:mysql://localhost/volunteer", "root", "");
										Statement statement1 = con.createStatement();
										resultset = statement1.executeQuery("select * from interests");
										Statement statement2 = con.createStatement();
										resultset2 = statement2.executeQuery("select * from skills");
										Statement statement3 = con.createStatement();
										resultset3 = statement3.executeQuery("select * from languages");
								%>


							<table id="causeareas" style="width:660px;">
								<tbody>
									<%
										int countca = 1;
										while (resultset.next()) {
											if (countca % 3 == 1) {
									%>
									<tr>
										<%
											}
										%>
										<td width="250px">
											<p>
												<input type="checkbox" name="interests"
													value=<%=resultset.getString(1)%>>&nbsp;&nbsp;<label
													class="skill"><%=resultset.getString(1)%></label>
											</p>
										</td>
										<%
											if (countca % 3 == 0) {
										%>
									</tr>
									<%
										}
									%>
									<%
										countca++;
										}
									%>									
									
									<%
										} catch (Exception e) {
											out.println("Wrong Entry: " + e);
										}
									%>

								</tbody>
							</table>

							</div>
						</div>
					
						<br>
						
						<div class="form-group">
							<label for="select" class="col-lg-2 control-label">Minimal Duration</label>
							<div class="col-lg-10">

								<select class="form-control" name="days">
									<%
										int duration = 1;
										while (duration < 366) {
									%>
									<option><%=duration++%></option>
									<%
										}
									%>
								</select> <br>
							</div>
						</div>						

						<div class="form-group">
							<label class="col-lg-2 control-label">I look for</label>
							<div class="col-lg-10">
								<div class="radio">
									<label> <input type="radio" name="lookingFor"
										id="optionsRadios1" value="accommodation">
										Accommodation
									</label>
								</div>
								<div class="radio">
									<label> <input type="radio" name="lookingFor"
										id="optionsRadios2" value="meals"> Meals
									</label>
								</div>

								<div class="radio">
									<label> <input type="radio" name="lookingFor"
										id="optionsRadios2" value="studying"> Studying
									</label>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="col-lg-10 col-lg-offset-2">								
								<button type="submit" class="btn btn-primary">Search</button>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>

		<%@include file="footer.jsp"%>

		<!-- container div end -->
	</div>

	<script src="js/jquery.autocomplete.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/volunteer.js"></script>


</body>
</html>
