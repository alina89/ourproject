<%@ page language="java" contentType="text/html; charset=windows-1255"
	pageEncoding="windows-1255"%>
<%@ page import="java.sql.*"%>
<%
	ResultSet resultset2 = null;
%>
<%
	ResultSet resultset3 = null;
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1255">
<title>Volunteer Advisor</title>

<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/signup.css" rel="stylesheet">
</head>

<body>
	<!-- Navigation: add to body -->
	<nav class="navbar navbar-inverse navbar-fixed-top" id="header"
		role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.jsp"><i
				class="fa fa-globe fa-1x"></i> Volunteer Advisor</a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="recommend.jsp">Suggest a Place</a></li>
				<li><a href="volunteer.jsp">Find a Place</a></li>				
				<li><a href="contact.jsp">Contact Us</a></li>
				<li><a href="signin.jsp" style="margin-left: 280px">Log In</a></li>
				<li><a href="signup.jsp">Sign Up</a></li>
			</ul>
		</div>
	</div>
	</nav>
	<!----------------->

	<div style="margin-top:110px" class="container">
		<div class="row">
			<div
				class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
				<form action="SignInServlet"">
					<h2>Please Sign In</h2>
					<hr class="colorgraph">

					<!-- Display Name -->
					<div class="form-group">
						<input type="text" name="display_name" id="display_name"
							class="form-control input-lg" placeholder="Display Name"
							tabindex="1">
					</div>

					<!-- Password -->
					<div class="form-group">
						<input type="password" name="password" id="password"
							class="form-control input-lg" placeholder="Password" tabindex="2">
					</div>


					<!-- Register or Sign in -->
					<hr class="colorgraph">
					<div class="row">
						<div class="col-xs-12 col-md-6">
							<input type="submit" value="Sign in"
								class="btn btn-primary btn-block btn-lg" tabindex="5">
						</div>
						<div class="col-xs-12 col-md-6">
							<a href="index.jsp" class="btn btn-success btn-block btn-lg">Cancel</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/signup.js"></script>

</body>
</html>