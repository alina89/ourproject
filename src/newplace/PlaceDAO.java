package newplace;

import java.io.InputStream;
import java.sql.*;

public class PlaceDAO {
	static Connection currentCon = null;
	static ResultSet rs = null;
	static ResultSet rs1 = null;

	public static PlaceBean login(PlaceBean bean) {

		// preparing some objects for connection: placename, phone,email,
		// country,
		// city, age, minimal duration, maximal duration, languages, skills,
		// causeAreas, provisions(benefits)
		Statement stmt = null;
		Statement stmt1 = null;

		String placename = bean.getPlacename();
		String phone = bean.getPhone();
		String email = bean.getEmail();
		String country = bean.getCountry();
		String city = bean.getCity();
		String age_start = bean.getMinimalAge();
		String days_start = bean.getDaysStart();
		String days_end = bean.getDaysEnd();
		InputStream image = bean.getImage();

		String review = bean.getReview();
		// int user_id = bean.getUserId();

		int score = bean.getScore();

		String[] langauge = bean.getLangs();
		String[] skills = bean.getSkills();
		String[] areas = bean.getAreas();
		String[] provisions = bean.getProvisions();

		String user_name = bean.getUserName();

		String searchQuery1 = "select `id` from users where username='"
				+ user_name + "'";
		try {
			// connect to DB
			currentCon = ConnectionManage.getConnection();
			stmt1 = currentCon.createStatement();
			rs1 = stmt1.executeQuery(searchQuery1);
			// boolean more = rs1.next();
			while (rs1.next()) {

				int user_id = rs1.getInt("id");
				bean.setUserId(user_id);
				System.out.println(user_id);
			}

		}

		catch (Exception ex) {
			System.out
					.println("finding user_id has failed: An Exception has occurred! "
							+ ex);
		}

		// some exception handling
		finally {
			if (rs1 != null) {
				try {
					rs1.close();
				} catch (Exception e) {
				}
				rs1 = null;
			}

			if (stmt1 != null) {
				try {
					stmt1.close();
				} catch (Exception e) {
				}
				stmt1 = null;
			}

			if (currentCon != null) {
				try {
					currentCon.close();
				} catch (Exception e) {
				}

				currentCon = null;
			}
		}

		// String searchQuery = "select * from users where username='" +
		// username + "' AND password='" + password + "'";
		// /////////////////should find a way to identify user and add his id to
		// another column in DB//////////
		// ////////////////for algorithm purpose/////////////////////////

		int user_id = bean.getUserId();

		String searchQuery = "select * from places where name='" + placename
				+ "'";

		try {
			// connect to DB
			currentCon = ConnectionManage.getConnection();
			stmt = currentCon.createStatement();
			rs = stmt.executeQuery(searchQuery);
			boolean more = rs.next();

			// if place does not exist add it

			if (!more) {
				System.out.println("Place was not found, add it");

				String insertPlaceString = "INSERT INTO places (`name`,`phone`,`email`,`country`,`city`,`area`,`skills`,`age_start`,`looking_for`,`days_start`,`days_end`, `image`,`lang`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";

				PreparedStatement stmtAddPlace = currentCon.prepareStatement(
						insertPlaceString, Statement.RETURN_GENERATED_KEYS);

				stmtAddPlace.setString(1, placename);
				stmtAddPlace.setString(2, phone);
				stmtAddPlace.setString(3, email);
				stmtAddPlace.setString(4, country);
				stmtAddPlace.setString(5, city);
				stmtAddPlace.setString(8, age_start);
				stmtAddPlace.setString(10, days_start);
				stmtAddPlace.setString(11, days_end);
				
				// skills - multiple
				String allskills = "";
				if (skills != null) {
					int count = 0;
					for (int i = 0; i < skills.length; i++) {
						String real_Skill = "";
						String query = "SELECT * FROM skills WHERE id="
								+ skills[i];
						rs = stmt.executeQuery(query);
						if (rs.next()) {
							count++;
							real_Skill = rs.getString("skill");
							// first one or the only one
							if (count == 1) {
								allskills = allskills + "|" + real_Skill + "|";
							} else {
								allskills = allskills + " " + real_Skill + "|";
							}
						}
					}
				}
				stmtAddPlace.setString(7, allskills);

				// language - multiple
				String alllangauge = "";
				if (langauge != null) {
					int count = 0;
					for (int i = 0; i < langauge.length; i++) {
						if (count == 1) {
							alllangauge = alllangauge + "|" + langauge[i] + "|";
						} else {
							alllangauge = alllangauge + " " + langauge[i] + "|";
						}
					}
				}
				stmtAddPlace.setString(13, alllangauge);

				// areas - multiple
				String allareas = "";
				if (areas != null) {
					int count = 0;
					for (int i = 0; i < areas.length; i++) {
						String real_Area = "";
						String query = "SELECT * FROM interests WHERE id="
								+ areas[i];
						rs = stmt.executeQuery(query);
						if (rs.next()) {
							count++;
							real_Area = rs.getString("area");
							// first one or the only one
							if (count == 1) {
								allareas = allareas + "|" + real_Area + "|";
							} else {
								allareas = allareas + " " + real_Area + "|";
							}
						}
					}
				}
				stmtAddPlace.setString(6, allareas);

				// benefits - multiple
				String allbenefits = "";
				if (provisions != null) {
					int count = 0;
					for (int i = 0; i < provisions.length; i++) {
						String real_Prov = "";
						String query = "SELECT * FROM benefits WHERE  benefit like \""
								+ provisions[i] + "\"";
						rs = stmt.executeQuery(query);
						if (rs.next()) {
							count++;
							real_Prov = rs.getString("benefit");
							// first one or the only one
							if (count == 1) {
								allbenefits = allbenefits + "|" + real_Prov
										+ "|";
							} else {
								allbenefits = allbenefits + " " + real_Prov
										+ "|";
							}
						}
					}
				}
				stmtAddPlace.setString(9, allbenefits);

				// adding the image
				if (image != null) {
					// fetches input stream of the upload file for the blob
					// column
					stmtAddPlace.setBinaryStream(12, image);
					// stmtAddPlace.setBlob(12, image);
				}

				// sends the statement to the database server
				stmtAddPlace.executeUpdate();
				ResultSet rs = stmtAddPlace.getGeneratedKeys();
				rs.next();

				int newId = rs.getInt(1);
				bean.setPlacId(newId);
				int place_id = bean.getPlaceId();
				
				
				
				String insertReviewString = "INSERT INTO reviews (`user_id`,`score`,`review`,`place_id`) VALUES (?,?,?,?)";

				PreparedStatement stmtAddReview = currentCon.prepareStatement(
						insertReviewString, Statement.RETURN_GENERATED_KEYS);

				stmtAddReview.setInt(1, user_id);
				stmtAddReview.setInt(2, score);
				stmtAddReview.setString(3, review);
				stmtAddReview.setInt(4, place_id);

				stmtAddReview.executeUpdate();

			}

			// if place already exists
			else {
				System.out
						.println("Place was found in the DB,cannot add place more than once");
				// bean.setExists(true);
			}

		} catch (Exception ex) {
			System.out
					.println("uploading new place has failed: An Exception has occurred! "
							+ ex);
		}

		// some exception handling
		finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
				}
				rs = null;
			}

			if (stmt != null) {
				try {
					stmt.close();
				} catch (Exception e) {
				}
				stmt = null;
			}

			if (currentCon != null) {
				try {
					currentCon.close();
				} catch (Exception e) {
				}

				currentCon = null;
			}
		}

		return bean;

	}
}
