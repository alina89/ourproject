<footer id="footer">
	<div class="row">
		<div class="col-lg-12">
			<hr>
			<p>Copyright &copy; Alina Dulchevsky, Netalie Stup, Tali
				Goldshtein &amp; Karen Shaham</p>
		</div>
	</div>
</footer>