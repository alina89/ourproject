package signUp;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter; 
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



public class SignInServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	 
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SignInServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			UserBean user = new UserBean();
			user.setUsername(request.getParameter("display_name"));
			user.setPassword(request.getParameter("password"));			
			
			user = UserDAOin.login(user);

			if (!user.exists()) {							
				response.sendRedirect("invalidLogin2.jsp"); // error page
			}

			else {
				//HttpSession session = request.getSession(true);
				//session.setAttribute("currentSessionUser", user);	
				
				String username = request.getParameter("display_name");							
				Cookie loginCookie = new Cookie("user",username);
				Cookie userIdCookie = new Cookie("user_id", String.format("%d", user.getId()));
		        //setting cookie to expiry in 30 mins
		        loginCookie.setMaxAge(30*60);
		        response.addCookie(loginCookie);
		        response.addCookie(userIdCookie);
		        
		        String from_page = request.getParameter("from");
				response.sendRedirect("index.jsp"); // success page		        
			}
		} catch (Throwable theException) {
			System.out.println(theException);
		}
	}

}