-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 16, 2015 at 10:37 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `volunteer`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `username` varchar(2048) NOT NULL,
  `password` varchar(2048) NOT NULL,
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `country` varchar(2048) NOT NULL,
  `city` varchar(2048) NOT NULL,
  `age` varchar(2048) NOT NULL,
  `langs` varchar(2048) NOT NULL,
  `skills` varchar(2048) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=45 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`username`, `password`, `id`, `country`, `city`, `age`, `langs`, `skills`) VALUES
('alina', '111', 1, 'israel', 'Rishon Le Ziyyon', '25', '|English|', '|Data Entry|'),
('karen', '5454', 19, 'israel', 'HOLON', '19', '|Animal Care / Handling| Fundraising| Verbal / Written Communication| Customer Service| Accounting|', ' Hebrew| Arabic| French| Chinese|'),
('netalie', '1212', 21, 'israel', 'holon', '19', '|Web Design| Management|', ' Portuguese| Russian|'),
('bbb', '', 22, '', '', '8', '', ''),
('kid', '111', 23, '', '', '8', '', ''),
('karenshaham', '123456', 24, 'Israel', 'Tel Aviv', '18', '|Administrative Support| Web Design|', ' English|'),
('Tali', '111', 25, 'Israel', 'Rishon Le Ziyyon', '25', ' English|', '|Data Entry|'),
('Emma', '111', 26, 'Israel', 'Rishon Le Ziyyon', '18', ' English|', '|Data Entry|'),
('Arielle', '111', 27, 'Israel', 'Rishon Le Ziyyon', '20', ' English|', '|Administrative Support| Farming|'),
('inbar', '111', 28, 'United Kingdom', 'London', '25', ' English|', '|Web Design| Management|'),
('Tom', '111', 29, 'Israel', 'Rishon Le Ziyyon', '21', ' English| Hebrew|', '|Data Entry|'),
('all', '111', 30, 'United States', 'San Francisco', '27', ' English| Hebrew| Spanish| Arabic| Portuguese| French| Russian|', '|Administrative Support| Animal Care / Handling| Data Entry| Social Media / Blogging| Fundraising| Verbal / Written Communication| Basic Computer Skills| Customer Service| Accounting| Photography| Web Design| Video Production| Computer Science| Business Development & Sales | Management| Sales Process| Mobile Programming| Cooking / Catering| Teaching / Instruction| Problem Solving| Sports Coaching| Mentoring| Youth Activities Management| Knitting| Food & Beverage Services| Farming| Food Delivery / Distribution|'),
('Fabrizio', '111', 38, 'Italy', 'Milano', '75', ' English|', '|Data Entry|'),
('Yonni', '111', 39, 'Israel', 'Rishon Le Ziyyon', '24', ' English|', '|Data Entry|'),
('Shira', '111', 40, 'Israel', 'Rishon Le Ziyyon', '27', ' English|', '|Data Entry|'),
('Tamar', '111', 41, 'Israel', 'Rishon Le Ziyyon', '25', ' English|', '|Data Entry|'),
('Maurizio', '111', 42, 'Italy', 'Milano', '74', ' English|', '|Data Entry|'),
('Luigi', '111', 43, 'Italy', '111', '73', ' English|', '|Data Entry|'),
('Francesca', '111', 44, 'Italy', 'Milano', '72', ' English|', '|Data Entry|');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
