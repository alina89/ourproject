-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 16, 2015 at 10:37 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `volunteer`
--

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE IF NOT EXISTS `skills` (
  `skill` text NOT NULL,
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `skills`
--

INSERT INTO `skills` (`skill`, `id`) VALUES
('Administrative Support', 1),
('Animal Care / Handling', 2),
('Data Entry', 3),
('Social Media / Blogging', 4),
('Fundraising', 5),
('Verbal / Written Communication', 6),
('Basic Computer Skills', 7),
('Customer Service', 8),
('Accounting', 10),
('Photography', 11),
('Web Design', 12),
('Video Production', 13),
('Computer Science', 14),
('Management', 15),
('Business Development & Sales ', 16),
('Sales Process', 17),
('Mobile Programming', 18),
('Cooking / Catering', 20),
('Teaching / Instruction', 21),
('Problem Solving', 22),
('Sports Coaching', 23),
('Mentoring', 24),
('Youth Activities Management', 25),
('Knitting', 26),
('Food & Beverage Services', 27),
('Farming', 28),
('Food Delivery / Distribution', 29);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
