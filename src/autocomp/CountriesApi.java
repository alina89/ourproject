package autocomp;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

@WebServlet("/countries")
public class CountriesApi extends HttpServlet {
	private static final long serialVersionUID = 1L;

	//handles the countries request
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
			response.setContentType("application/json");
			List<Location> countries = LocationDAO.searchCountries(request.getParameter("query"));
			
			ObjectMapper mapper = new ObjectMapper();
			JsonFactory jfactory = mapper.getFactory();
			JsonGenerator jgen = jfactory.createGenerator(response.getOutputStream());
			jgen.writeStartObject();
			jgen.writeArrayFieldStart("suggestions");
			for (Location location : countries) {
				ObjectNode node = mapper.createObjectNode();
				node.put("value", location.getName());
				node.put("data", location.getId());

				jgen.writeObject(node);
			}
			jgen.writeEndArray();
			jgen.writeEndObject();
			jgen.flush();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace(response.getWriter());
		}
	}
	
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

	}

}
