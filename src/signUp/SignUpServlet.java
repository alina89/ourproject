package signUp;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	 
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SignUpServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			UserBean user = new UserBean();
			user.setUsername(request.getParameter("display_name"));
			user.setPassword(request.getParameter("password"));
			user.setCountry(request.getParameter("country"));
			user.setCity(request.getParameter("city"));
			user.setAge(request.getParameter("age"));			
			user.setLangs(request.getParameterValues("language"));
			user.setSkills(request.getParameterValues("skill"));
			
			user = UserDAO.login(user);

			if (!user.exists()) {
				String username = request.getParameter("display_name");							
				Cookie loginCookie = new Cookie("user", username);
				Cookie userIdCookie = new Cookie("user_id", String.format("%d", user.getId()));
		        
				//setting cookie to expiry in 30 mins
		        loginCookie.setMaxAge(30*60);
		        response.addCookie(loginCookie);
		        response.addCookie(userIdCookie);
				response.sendRedirect("index.jsp"); // success page
			}

			else {
				response.sendRedirect("invalidLogin.jsp"); // error page
			}
		} catch (Throwable theException) {
			System.out.println(theException);
		}
	}

}