package common;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.plaf.ScrollPaneUI;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.ArrayList;
import java.util.Iterator;

public class FindData extends HttpServlet  {
    private static final long serialVersionUID = 1L;
    

    protected void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {

        res.setContentType("application/json");

    
        
        String country = req.getParameter("country");
        String city = req.getParameter("city");
        String[] interests = req.getParameterValues("interests");
        String days = req.getParameter("days");
        String looking = req.getParameter("lookingFor");
        
        //myArray will hold all the outcomes of volunteering places IDs offered to the user
        ArrayList<String> myArray = new ArrayList<String>();
        ArrayList<String> usersRecommendedi = new ArrayList<String>();
        ArrayList<Float> ratingArr = new ArrayList<Float>();
        
        

        // get those from the session
        String age;
        String[] language;
        String[] skills;
        String userCountry;
        String userTown;
        
        int totalSkills = 27; 

        String userName = null;
        Cookie[] cookies = req.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("user"))
                    userName = cookie.getValue();
            }
        }
        
        try {
            Connection con = new SqlConnect().getConn();
            String query = "SELECT * FROM users WHERE (username=?)";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, userName);
            
            ResultSet rs = ps.executeQuery();

            rs.next();
            age = rs.getString("age");
            userCountry= rs.getString("country");
            userTown = rs.getString("city");
            String lan = rs.getString("langs");
            language = lan.substring(1, lan.length() - 1).split("\\|");

            String skill = rs.getString("skills");
            skills = skill.substring(1, skill.length() - 1).split("\\|");

            query = "SELECT * FROM places "
                    + "WHERE (country=? OR ? IS NULL) "
                    + "AND (city=? OR ? IS NULL)"
                    + "AND ((age_start<?) OR ?=8)"
                    + "AND ((days_start<? and days_end>?) OR ?=1)"
                    + "AND (looking_for=? or ? IS NULL)"; // 11

            con = new SqlConnect().getConn();

            // interests - multiple
            int placeholder_count = 12;
            if (interests != null) {

                query = query + "AND";
                for (int i = 0; i < interests.length; i++) {

                    if (i == 0 && interests.length == 1) {
                        query = query + " (area LIKE (?))";
                    } else if (i == 0) {
                        query = query + " (area LIKE (?) OR";
                    } else if (i == interests.length - 1) {
                        query = query + " area LIKE (?))";
                    } else {
                        query = query + " area LIKE (?) OR";
                    }
                }
            } else {
                query += " AND (? IS NULL)";
            }

            // skills - multiple
            if (skills != null) {

                query = query + "AND";
                for (int i = 0; i < skills.length; i++) {

                    if (i == 0 && skills.length == 1) {
                        query = query + " (skills LIKE (?))";
                    } else if (i == 0) {
                        query = query + " (skills LIKE (?) OR";
                    } else if (i == skills.length - 1) {
                        query = query + " skills LIKE (?))";
                    } else {
                        query = query + " skills LIKE (?) OR";
                    }
                }
            } else {
                query += " AND (? IS NULL)";
            }

            // language - multiple
            if (language != null) {

                query = query + "AND";
                for (int i = 0; i < language.length; i++) {

                    if (i == 0 && language.length == 1) {
                        query = query + " (lang LIKE (?))";
                    } else if (i == 0) {
                        query = query + " (lang LIKE (?) OR";
                    } else if (i == language.length - 1) {
                        query = query + " lang LIKE (?))";
                    } else {
                        query = query + " lang LIKE (?) OR";
                    }
                }
            } else {
                query += " AND (? IS NULL)";
            }

            ps = con.prepareStatement(query);

            if (interests != null) {
                for (int i = 0; i < interests.length; i++) {
                    ps.setString(placeholder_count++, "%" + interests[i] + "%");
                }
            } else {
                ps.setString(placeholder_count++, null);
            }

            if (skills != null) {
                for (int i = 0; i < skills.length; i++) {
                    ps.setString(placeholder_count++, "%" + skills[i] + "%");
                }
            } else {
                ps.setString(placeholder_count++, null);
            }

            if (language != null) {
                for (int i = 0; i < language.length; i++) {
                    ps.setString(placeholder_count++, "%" + language[i] + "%");
                }
            } else {
                ps.setString(placeholder_count++, null);
            }

            // country - simple
            if (country == "") {
                ps.setString(1, null);
                ps.setString(2, null);
            } else {
                ps.setString(1, country);
                ps.setString(2, country);
            }
            // city - simple
            if (city == "") {
                ps.setString(3, null);
                ps.setString(4, null);
            } else {
                ps.setString(3, city);
                ps.setString(4, city);
            }
            // age - simple
            ps.setString(5, age);
            ps.setString(6, age);

            ps.setString(7, days);
            ps.setString(8, days);
            ps.setString(9, days);

            ps.setString(10, looking);
            ps.setString(11, looking);

            rs = ps.executeQuery();
            ObjectMapper mapper = new ObjectMapper();
            JsonFactory jfactory = mapper.getFactory();
            JsonGenerator jgen = jfactory
                    .createGenerator(res.getOutputStream());
            jgen.writeStartArray();

            //Will go over all the outcomes of volunteering places offered to user 
            while (rs.next()) {
                /**
                ObjectNode node = mapper.createObjectNode();
                node.put("name", rs.getString("name"));
                node.put("phone", rs.getString("phone"));
                node.put("country", rs.getString("country"));
                node.put("city", rs.getString("city"));

                ArrayNode areaJson = node.putArray("area");
                String areaStr = rs.getString("area");
                String[] areaArr = areaStr.substring(1, areaStr.length() - 1).split("\\|");
                node.putArray("area");
                for (String area : areaArr) {
                    areaJson.add(area);
                }

                ArrayNode skillsJson = node.putArray("skills");
                String skillsStr = rs.getString("skills");
                String[] skillsArr = skillsStr.substring(1,    skillsStr.length() - 1).split("\\|");
                for (String oneskill : skillsArr) {
                    skillsJson.add(skill);
                }

                ArrayNode langJson = node.putArray("lang");
                String langStr = rs.getString("lang");
                String[] langArr = langStr.substring(1, langStr.length() - 1).split("\\|");
                for (String lang : langArr) {
                    langJson.add(lang);
                }

                
                node.put("looking_for", rs.getString("looking_for"));                
                node.put("image_url", rs.getString("image"));
                node.put("age_start", rs.getString("age_start"));                
                node.put("days_start", rs.getString("days_start"));
                node.put("days_end", rs.getString("days_end"));
                node.put("id", rs.getString("id"));
                **/
                myArray.add(rs.getString("id"));
                
            }
    
            String query2;
            String queryReviews;
            String a, c;
            PreparedStatement ps2;
            PreparedStatement psReviews;
            ResultSet rs2;
            ResultSet rsReviews;   
            
            //Connection con2 = new SqlConnect().getConn();
            for( int i=0; i< myArray.size();i++){
                
                a =  myArray.get(i);
                
                queryReviews = "SELECT * FROM reviews WHERE (place_id =" +a+" )";
                psReviews = con.prepareStatement(queryReviews);
                rsReviews = psReviews.executeQuery();
                ;
                

                //Go over all the lines of reviews for the specific volunteering place 
                while(rsReviews.next())
                {
                    
                    //usersRecommendedi Will Hold a List of all Users that Has Recommended this specific Volunteering place
                    usersRecommendedi.add(rsReviews.getString("user_id"));
                }//End of "while"- Going over all the users who has recommended this specific Volunteering place.
                
                float wUser;
                float sumWuser = 0;
                float ratingUser;
                float score;
                float sumScore = 0;
                float locationRatingPerUser;
                String ratersLanguage;
                String ratersCountry;
                String ratersSkills;
                int ratersAge;
                String ratersTown;
                String ratersQuery; 
                PreparedStatement psRater;
                ResultSet rsRater;
                int internationalLanguageCnt;
                int otherLanguageCnt;
                
                for( int j=0; j< usersRecommendedi.size();j++)
                {
                    internationalLanguageCnt=0;
                    otherLanguageCnt = 0;
                    
                    c = usersRecommendedi.get(j);
                    ratersQuery = "SELECT * FROM users WHERE (id =" +c+" )";
                    psRater = con.prepareStatement(ratersQuery);
                    rsRater = psRater.executeQuery();
                    
                    while(rsRater.next()){
                        
                        //AGE
                        ratersAge = Integer.parseInt(rsRater.getString("age"));
                        int userAge = Integer.parseInt(age);
                        wUser = (float)(java.lang.Math.min(ratersAge, userAge))/(float)(java.lang.Math.max(ratersAge, userAge));
                        
                        
                        //COUNTRY
                        ratersCountry = rsRater.getString("country");
                        ratersCountry = ratersCountry.toLowerCase();
                        userCountry = userCountry.toLowerCase();
                        
                        float ans = (float)(ratersCountry.equals(userCountry)? 1.0: 0.0 );
                        wUser += ( ((3.0)*ans)/ (5.0) );
                        
                        
                        //TOWN
                        ratersTown = rsRater.getString("city");
                        ratersTown = ratersTown.toLowerCase();
                        userTown = userTown.toLowerCase();
                        
                        ans = (float)(ratersTown.equals(userTown)? 1.0: 0.0 );
                        wUser+= ( ((2.0)*ans)/ (5.0) );
                        
                        
                        //LANGUAGES
                        
                        ratersLanguage = rsRater.getString("langs");
                        if(!ratersLanguage.isEmpty()){
                            String[] ratersLangArr = ratersLanguage.substring(1, ratersLanguage.length() - 1).split("\\|");
                            
                        
                            for ( int x = 0; x < language.length ; x++ )
                            {
                                for (int k = 0 ; k < ratersLangArr.length ; k++ )
                                {
                                    String lan1 = language[x].toLowerCase() ;
                                    String lan2 = ratersLangArr[k].toLowerCase();
                                    if ( lan1.equals( lan2 ) )
                                    {
                                        if ( lan1.equals("english") || lan1.equals("spanish") || lan1.equals("french"))
                                        {
                                            internationalLanguageCnt = 1;    
                                        }//if
                                        else otherLanguageCnt =1;
                                    }//if
                                }//for k
                            }// for x
                        
                            float lanSum = (float)((4.0*(float)internationalLanguageCnt)/5.0) + (float)((2.0*(float)otherLanguageCnt)/5.0);
                            wUser += java.lang.Math.min(lanSum ,1.0 );
                            
                        }
                        
                        // SKILLS
                        
                        //Will sum up all the common Skills of user and rater
                        int sameSkill = 0 ;
                        int differentSkills =0; 
                        String[] ratersSkillsArr;
                        
                        ratersSkills = rsRater.getString("skills");
                        if(!ratersSkills.isEmpty())
                        {
                            ratersSkillsArr = ratersSkills.substring(1, ratersSkills.length() - 1).split("\\|");
                            
                    
                            for ( int x = 0; x < skills.length ; x++ )
                            {
                                for (int k = 0 ; k < ratersSkillsArr.length ; k++ )
                                {
                                    String skl1 = skills[x].toLowerCase().replaceAll("\\s+","");
                                    String skl2 = ratersSkillsArr[k].toLowerCase().replaceAll("\\s+","");
                                    if ( skl1.equalsIgnoreCase( skl2 ) )
                                    {            
                                            sameSkill++;
                                    }//if
                                }//for k
                            }// for x
                        
                            differentSkills = totalSkills - ( skills.length + ratersSkillsArr.length - sameSkill);
                            //empty ratersSkillsArr
                            ratersSkillsArr = null;
                        }
                            
                        else
                        {
                            differentSkills = totalSkills - ( skills.length  - sameSkill)  ;
                        }
                            wUser += (float)((float)(differentSkills+sameSkill)/(float)(totalSkills));
                            
                        
                        wUser = (float)(wUser / 4.0);
                        
                        
                        String queryReviews2 = "SELECT * FROM reviews  "  
                        + "WHERE (user_id =" +c+"  )"
                        + "AND   (place_id = " +myArray.get(i)+ " )" ;
                        PreparedStatement psReviews2 = con.prepareStatement(queryReviews2);
                        ResultSet rsReviews2 = psReviews2.executeQuery();
                        
                        rsReviews2.next();
                        ratingUser =(float) rsReviews2.getInt("score");
                        
                        
                        score = ratingUser*wUser;
                        sumScore+= score;
                        
                        sumWuser += wUser;     
                    
                }//End of "while"
                
            } // End of "for j" - Going over all recommenders of specific place  ;
                
                locationRatingPerUser = (float) ( sumScore / sumWuser);
                ratingArr.add(locationRatingPerUser); 
                
                //empty usersRecommendedi
                for (Iterator<String> iter = usersRecommendedi.listIterator(); iter.hasNext(); ) 
                {
                    String b = iter.next();
                    iter.remove();
                }
                
        }//End of "for"- Going over all the Volunteering places found matching to the user.
            
            String [] placesArr = new String [myArray.size()];
            Float [] ratingArray  = new Float [ratingArr.size()];
        
            for(int i = 0; i < myArray.size(); i++)
            {
                placesArr[i]   = myArray.get(i);
                ratingArray[i] = ratingArr.get(i); 
            }
            
          
           //Sort
            float tempRating;
            String tempPlaces;
            
           for (int y=0; y< ratingArray.length-1 ; y++)
           {
               for(int t = y+1 ; t < ratingArray.length ; t++  )
               {
                   if (ratingArray[t]>ratingArray[y])
                   {
                      tempRating=  ratingArray[t];
                      ratingArray[t] = ratingArray[y];
                      ratingArray[y] = tempRating;
                      
                      tempPlaces=  placesArr[t];
                      placesArr[t] = placesArr[y];
                      placesArr[y] = tempPlaces;
                   }
               }
           }
           
            
            
        for(int i = 0; i < myArray.size(); i++){
            
            String place_id = placesArr[i];
            String repQuery = "SELECT * FROM places WHERE (ID =" +place_id+" )";
            PreparedStatement repPs = con.prepareStatement(repQuery);
            ResultSet repRs = repPs.executeQuery();
            
            
            repRs.next();
            
            System.out.println("The Score for "+repRs.getString("name")+" is " +ratingArray[i]+"\n");
            
            ObjectNode node = mapper.createObjectNode();
            node.put("name", repRs.getString("name"));
            node.put("phone", repRs.getString("phone"));
            node.put("country", repRs.getString("country"));
            node.put("city", repRs.getString("city"));

            ArrayNode areaJson = node.putArray("area");
            String areaStr = repRs.getString("area");
            String[] areaArr = areaStr.substring(1, areaStr.length() - 1).split("\\|");
            node.putArray("area");
            for (String area : areaArr) {
                areaJson.add(area);
            }

            ArrayNode skillsJson = node.putArray("skills");
            String skillsStr = repRs.getString("skills");
            String[] skillsArr = skillsStr.substring(1,    skillsStr.length() - 1).split("\\|");
            for (String oneskill : skillsArr) {
                skillsJson.add(skill);
            }

            ArrayNode langJson = node.putArray("lang");
            String langStr = repRs.getString("lang");
            String[] langArr = langStr.substring(1, langStr.length() - 1).split("\\|");
            for (String lang : langArr) {
                langJson.add(lang);
            }

            
            node.put("looking_for", repRs.getString("looking_for"));                
            node.put("image_url", repRs.getString("image"));
            node.put("age_start", repRs.getString("age_start"));                
            node.put("days_start", repRs.getString("days_start"));
            node.put("days_end", repRs.getString("days_end"));
            node.put("id", repRs.getString("id"));
            
            jgen.writeObject(node);
            }//End of "for"
        
            jgen.writeEndArray();
            jgen.flush();
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

    }// End of doGet
    
    
}// end of Class