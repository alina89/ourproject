package places;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import common.SqlConnect;

@WebServlet("/places")
public class PlacesApi extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		res.setContentType("application/json");

		String country = req.getParameter("country");
		String city = req.getParameter("city");
		String interest = req.getParameter("interests");

		try {
			String query = "SELECT *, avg(r.score) avgRating "
					+ "FROM places p "
					+ "LEFT JOIN reviews r on r.place_id = p.id "
					+ "WHERE (country=? OR ? IS NULL) "
					+ "AND (city=? OR ? IS NULL) "
					+ "AND (area like ? OR ? is NULL) " 
					+ "GROUP BY place_id "
					+ "ORDER BY avg(r.score) desc";

			Connection con = new SqlConnect().getConn();
			PreparedStatement ps = con.prepareStatement(query);

			String interestParam = interest == null ? null : String.format("%%%s%%", interest);
			
			ps.setString(1, country);
			ps.setString(2, country);
			ps.setString(3, city);
			ps.setString(4, city);
			ps.setString(5, interestParam);
			ps.setString(6, interestParam);
			
			ResultSet rs = ps.executeQuery();
			ObjectMapper mapper = new ObjectMapper();
			JsonFactory jfactory = mapper.getFactory();
			JsonGenerator jgen = jfactory.createGenerator(res.getOutputStream());
			jgen.writeStartArray();
			while (rs.next()) {
				ObjectNode node = mapper.createObjectNode();
				node.put("name", rs.getString("name"));
				node.put("phone", rs.getString("phone"));
				node.put("country", rs.getString("country"));
				node.put("city", rs.getString("city"));

				ArrayNode areaJson = node.putArray("area");
				String areaStr = rs.getString("area");
				String[] areaArr = areaStr.substring(1, areaStr.length() - 1).split("\\|");
				node.putArray("area");
				for (String area : areaArr) {
					areaJson.add(area);
				}

				ArrayNode skillsJson = node.putArray("skills");
				String skillsStr = rs.getString("skills");
				String[] skillsArr = skillsStr.substring(1,	skillsStr.length() - 1).split("\\|");
				for (String oneskill : skillsArr) {
					skillsJson.add(oneskill);
				}

				ArrayNode langJson = node.putArray("lang");
				String langStr = rs.getString("lang");
				String[] langArr = langStr.substring(1, langStr.length() - 1).split("\\|");
				for (String lang : langArr) {
					langJson.add(lang);
				}

				
				node.put("looking_for", rs.getString("looking_for"));				
				node.put("image_url", rs.getString("image"));
				node.put("age_start", rs.getString("age_start"));				
				node.put("days_start", rs.getString("days_start"));
				node.put("days_end", rs.getString("days_end"));
				node.put("id", rs.getString("id"));

				jgen.writeObject(node);
			}
			jgen.writeEndArray();
			jgen.flush();

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace(res.getWriter());
		}
	}
}
