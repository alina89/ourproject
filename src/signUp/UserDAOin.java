package signUp;

import java.sql.*;

public class UserDAOin {
	static Connection currentCon = null;
	static ResultSet rs = null;	

	public static UserBean login(UserBean bean) {

		// preparing some objects for connection: username & password
		Statement stmt = null;	
		
		String username = bean.getUsername();
		String password = bean.getPassword();		

		String searchQuery = "select * from users where username='" + username + "' AND password='" + password + "'";
		
				
		try {
			// connect to DB
			currentCon = ConnectionManager.getConnection();
			stmt = currentCon.createStatement();			
			rs = stmt.executeQuery(searchQuery);
			boolean more = rs.next();

			// if user does not exist set the isValid variable to false and let the user know that he needs to sign up
			if (!more) {
				System.out.println("User was not found, wrong");
				bean.setExists(false);	
			}	

			// if user exists set the isValid variable to true
			else {
				System.out.println("User was found in the DB!");				
				bean.setExists(true);
				bean.setId(rs.getInt("id"));
			}
		} catch (Exception ex) {
			System.out.println("Log In failed: An Exception has occurred! "	 + ex);
		}

		// some exception handling
		finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
				}
				rs = null;
			}

			if (stmt != null) {
				try {
					stmt.close();
				} catch (Exception e) {
				}
				stmt = null;
			}

			if (currentCon != null) {
				try {
					currentCon.close();
				} catch (Exception e) {
				}

				currentCon = null;
			}
		}

		return bean;
	}
}