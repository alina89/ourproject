package reviews;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.naming.AuthenticationException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import autocomp.Location;
import autocomp.LocationDAO;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Servlet implementation class auto
 */
@WebServlet("/reviews")
public class ReviewsApi extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ReviewsApi() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int placeId = Integer.parseInt(request.getParameter("placeId"));
		int score = Integer.parseInt(request.getParameter("score"));
		String review = request.getParameter("review");
		int userId = -1;
		
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals("user_id"))
					userId = Integer.parseInt(cookie.getValue());
			}
		}
		
		if (userId == -1)
			throw new ServletException("user not authenticated");

		try {
			response.setContentType("application/json");
			ReviewBean reviewBean = ReviewDAO.saveReview(placeId, review, score, userId);
			
			ObjectMapper mapper = new ObjectMapper();
			JsonFactory jfactory = mapper.getFactory();
			JsonGenerator jgen = jfactory.createGenerator(response.getOutputStream());
			jgen.writeObject(reviewBean);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace(response.getWriter());
		} 
	}
}
