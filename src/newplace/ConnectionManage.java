package newplace;
import java.sql.*;

public class ConnectionManage {

	static Connection con;
	static String url;

	public static Connection getConnection() throws InstantiationException, IllegalAccessException {

		try {
			String url = "jdbc:mysql://localhost/volunteer";
			Class.forName("com.mysql.jdbc.Driver").newInstance();

			try {
				con = DriverManager.getConnection(url, "root", "");				

			}

			catch (SQLException ex) {
				ex.printStackTrace();
			}
		}

		catch (ClassNotFoundException e) {
			System.out.println(e);
		}

		return con;
	}
}
