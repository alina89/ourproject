package signUp;

public class UserBean {	
	
	private int id;
	private String username;
	private String password;
	private String country;
	private String city;
	private String age;
	private String[] langs;
	private String[] skills;
	private boolean exist;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}	
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String newUsername) {
		username = newUsername;
	}
		

	public String getPassword() {
		return password;
	}

	public void setPassword(String newPassword) {
		password = newPassword;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String newCountry) {
		country = newCountry;
	}
	
	public String getCity() {
		return city;
	}

	public void setCity(String newCity) {
		city = newCity;
	}
	
	public String getAge() {
		return age;
	}

	public void setAge(String newAge) {
		age = newAge;
	}
	
	public String[] getLangs() {
		return langs;
	}

	public void setLangs(String[] newLangs) {
		langs = newLangs;
	}
	
	public String[] getSkills() {
		return skills;
	}

	public void setSkills(String[] newSkills) {
		skills = newSkills;
	}
		

	public boolean exists() {
		return exist;
	}

	public void setExists(boolean newValid) {
		exist = newValid;
	}
}